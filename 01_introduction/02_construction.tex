%!TEX root = ../main.tex
\subsection[Конструкция рассматриваемых пространств]{\textls[-5]{Конструкция рассматриваемых пространств}}
\label{sec:introduction:construction}

Определим множества функций
\begin{gather*}
    P_1 = \l\{
        f \in \omega^\omega \colon
            0 \< f(n) \< n+1
            \ \text{для всех}\ n \in \omega
    \r\},\\
    P_2 = \l\{
        f \in \omega^\omega \colon
            0 \< f(n) \< 1
            \ \text{для всех}\ n \in \omega
    \r\},\\
    P_3 = \l\{
        f \in \omega^\omega \colon
            0 \< f(n) < \omega
            \ \text{для всех}\ n \in \omega
    \r\}.
\end{gather*}

В~качестве множеств на~которых будем строить булевы алгебры, возьмем сужения данных функций.
\begin{align*}
    \N{1} &= \l\{f|_n \colon f \in P_1, n \subseteq \omega\r\},\\
    \N{2} &= \l\{f|_n \colon f \in P_2, n \subseteq \omega\r\},\\
    \N{3} &= \l\{f|_n \colon f \in P_3, n \subseteq \omega\r\}.
\end{align*}

Каждое~\(\N{i}\) (\(i = 1, 2, 3\)) можно рассматривать как частично упорядоченное множество со~следующим отношением порядка: для~\(s, t \in \N{i}\) считаем, что~\(s < t\), если~\(t\) является продолжением~\(s\) (то есть~\(t|_{\dom s} = s\)). Дополним понятия цепи и~антицепи следующими определениями.
\begin{definition}
    \label{def:strict_antichain}
    \index{Антицепь!строгая}
    %
    Антицепь~\(A \subseteq \N{i}\) (\(i = 1, 2, 3\)) будем называть \emph{сторогой} антицепью, если для любых различных~\(s, t \in A\) выполнено~\(\dom s \neq \dom t\).
\end{definition}

\begin{definition}
    \label{def:full_chain}
    \index{Цепь!полная}
    %
    Цепь (антицепь) \(A \subseteq \N{i}\) (\(i = 1, 2, 3\)) будем называть \emph{полной} цепью (антицепью), если для всякого~\(n \in \omega \setminus \{0\}\) найдется~\(s \in A\) такое, что~\(\dom s = n\).
\end{definition}

Для~каждого~\(s \in \N{1}\) количество его продолжений на~следующий шаг растет с~ростом~\(\dom s\). Для~\(s \in \N{2}\) количество его продолжений на~следующий шаг всегда равно 2, а~для~\(s \in \N{3}\) оно всегда счетно.

Для~произвольного~\(s \in \N{i}\) (\(i = 1, 2, 3\)) определим
\[
    C_s = \l\{t \in \N{i} \colon t \in \N{i}, t\ \text{является продолжением}\ s\r\}.
\]

Рассмотрим два класса булевых алгебр на~множествах~\(\N{i}\) (\(i = 1, 2, 3\)). Булевы алгебры первого класса определим на~\(\N{i}\) (\(i=1, 2, 3\)) следующим образом.

Определим множество
\[
    T_i = \l\{
        \pi \in \N{i}^\omega \colon
            \dom \pi(n) = n+1\ \text{для всех}\ n \in \omega
    \r\}.
\]

Для~каждого~\(\pi \in T_i\) (\(i = 1, 2, 3\)) обозначим
\[
    C_\pi = \cup \l\{C_{\pi(n)} \colon n \in \omega\r\}.
\]

Обозначим через~\(\B{1, i}\) (\(i = 1, 2, 3\)) булеву алгебру, порождённую семейством
\[
    \mathfrak B'_i = \l\{C_\pi \colon \pi \in T_i\r\}.
\]

Определим пространство~\(\SB{1, i}\) как пространство Стоуна булевой алгебры~\(\B{1, i}\) (\(i = 1, 2, 3\)).

Докажем несколько лемм общих, для данных пространств.
\begin{proposition}
    \label{pro:Cs_in_bool}
    % \label{prop:1.2.1}
    %
    Для~каждого~\(s \in \N{i}\) справедливо~\(C_s \in \B{1, i}\) \textup(\(i = 1, 2, 3\)\textup).
\end{proposition}
\begin{proof}
    Пусть~\(s \in \N{i}\), докажем, что~\(C_s \in \B{1, i}\).

    Построим~\(\pi_0, \pi_1 \in T_i\) следующим образом:\\
    рассмотрим~\(f_0, f_1 \in P_2 \subseteq P_1 \subseteq P_3\) такие, что~\(f_j \equiv j\)~(\(j = 0, 1\)). Определим
    \[
        \pi_j(n)=
            \begin{cases}
                f_j|_{n+1},& \text{для}\ n \neq \dom s - 1;\\
                s,& \text{для}\ n = \dom s - 1,
            \end{cases}
            \qquad j = 0, 1.
    \]

    Так~как~\(f_0\) и~\(f_1\) различны, то, очевидно, ни~одно продолжение~\(\pi_0(n)\) не~может являться продолжением никакого~\(\pi_1(m)\) для~\(n, m \in \omega\), \(n \neq \dom s - 1\), \(m \neq \dom s - 1\).

    Таким образом, пересечение~\(C_{\pi_0} \cap C_{\pi_1}\) состоит из~элемента~\(\pi_0(\dom s - 1) = \pi_1(\dom s - 1) = s\) и~всех его продолжений. То~есть,
    \[
        C_{\pi_0} \cap C_{\pi_1} = C_s.
    \]

    Таким образом, \(C_s \in \B{1, i}\).
\end{proof}

\begin{proposition}
    \label{pro:s_in_bool}
    %
    Для~каждого~\(s \in \N{i}\) справедливо~\(\{s\} \in \B{1, i}\) \textup(\(i = 1, 2\)\textup).
\end{proposition}
\begin{proof}
    Поскольку для всякого~\(s \in \N{i}\) при~\(i = 1, 2\) количество продолжений его на~следующий шаг конечно, то~нетрудно показать, что для всякого~\(s \in \N{i}\) справедливо
    \[
        \{s\} = C_s \setminus \cup\{
            C_t \colon t \in \N{i}, s < t, \dom t = \dom s + 1
        \} \in \B{1, i}. \\[-0.6\baselineskip]
    \]
\end{proof}

Таким образом подространства фиксированных ультрафильтров~\(\N[\widehat]{1}\) и~\(\N[\widehat]{2}\) в~пространствах Стоуна булевых алгебр~\(\B{1, 1}\) и~\(\B{1, 2}\) являются дискретными. Подпространства~\(\N[\widehat]{1}\) и~\(\N[\widehat]{2}\) можно отождествить с~дискретными пространствами~\(\N{1}\) и~\(\N{2}\) соответсвенно, а~сами пространства Стоуна~\(\SB{1, 1}\) и~\(\SB{1, 2}\) рассматривать как бикомпактные расширения счетных дискретных пространств~\(\N{1}\) и~\(\N{2}\).

В~пространстве~\(\SB{1, 3}\) подпространство фиксированных ультрафильтров~\(\N[\widehat]{3}\) не~является дискретным.

\begin{proposition}
    \label{pro:full_base}
    % \label{prop:1.2.2}
    %
    Семейство множеств вида
    \[
        \L[
            \L(
                \capl{\pi\in T'} C_{\pi}
            \R) \cap \L(
                \capl{\pi\in T''} \N{i} \setminus C_{\pi}
            \R)
        \R],
        \quad\text{где}\quad
        T', T'' \subset T_i, |T'| < \omega, |T''| < \omega
    \]
    есть база пространства~\(\SB{1, i}\) \textup(\(i = 1, 2, 3\)\textup).
\end{proposition}
\begin{proof}
    Рассмотрим произвольную окресность произвольной точки~\(x \in U_A \subseteq \SB{1, i}\). Точка~\(x\) является ультрафильтром, состоящим из~элементов булевой алгебры~\(\B{1, i}\).

    Согласно предположению~\ref{pro:bool_dissection}, каждый элемент~\(A\) булевой алгебры~\(\B{1, i}\) представим в~виде
    \[
        A = (A_{0, 0} \cap \ldots \cap A_{0, r_0})
            \cup (A_{1, 0} \cap \ldots \cap A_{1, r_1})
            \cup \ldots \cup (A_{q, 0} \cap \ldots \cap A_{q, r_q}),
    \]
    где либо~\(A_{j, k} \in \mathfrak B'_i\), либо~\(\overline{A_{j, k}} \in \mathfrak B'_i\) для всех~\(k \< r_j\), \(j \< q\). А~значит найдётся~\(j \< q\) такое, что~\(A_{j, 0} \cap \ldots \cap A_{j, r_j}\) есть элемент ультрафильтра~\(x\) и, следовательно,
    \[
        x \in [(A_{j, 0} \cap \ldots \cap A_{j, r_j})] \subseteq [A] = U_A.\\[-0.6\baselineskip]
    \]
\end{proof}

Данные базы довольно громоздки и~неудобны в~работе. Рассмотрим другие базы для данных пространств. Для~их~построения приведем ряд вспомогательных результатов.

Для~произвольного~\(\pi \in T_i\) (\(i = 1, 2, 3\)) и~\(M \subseteq \omega\) определим множество~\(C_{\pi|M} = \cupl{n\in M}C_{\pi(n)}\).
\begin{proposition}
    \label{pro:CpiM_in_bool}
    % \label{prop:1.2.3}
    %
    Для~каждого~\(\pi \in T_i\) \textup(\(i = 1, 2, 3\)\textup) и~\(M \subseteq \omega\) найдутся~\(\pi_1, \pi_2 \in T_i\) такие, что~\(C_{\pi|M} = C_{\pi_1} \cap C_{\pi_2}\).
\end{proposition}
\begin{proof}
    Пусть~\(M = \{k_1, k_2, \ldots, k_j, \ldots\}\). Построим~\(\pi_1\) и~\(\pi_2 \in T_i\) следующим образом. Для~всех~\(0 \< n < k_1\) опрелелим~\(\pi_1(n)\) как тождественный~\(0\) на~множестве~\(n\), а~\(\pi_2(n)\) как тождественная~\(1\) на~множестве~\(n\). Для~\(k_j \< n < k_{j+1}\) \(\pi_1(n)\) и~\(\pi_2(n)\) определим как произвольные продолжения~\(\pi(k_j)\) на~\(n\) (при~\(n = k_j\) они будут совпадать с~\(\pi(k_j)\)).

    Из~построения~\(\pi_1\) и~\(\pi_2\) следует, что~\(C_{\pi|M} = C_{\pi_1} \cap C_{\pi_2}\). Таким образом, для произвольных~\(\pi \in T_i\) и~\(M \subseteq \omega\) справедливо~\(C_{\pi|M} \in \B{1, i}\).
\end{proof}

\begin{lemma}
    \label{lem:from_cap_to_cup}
    % \label{lem:1.2.1}
    %
    Пусть~\(\{\pi_j \colon j \< n\} \subseteq T_i\) (\(i = 1, 2, 3\)), \(n \in \omega\). Тогда для всякого семейства~\(\{C_{\pi_j|M_j} \colon j \< n\}\) верно следующее:
    \begin{gather*}
        \capl{j \< n}C_{\pi_j|M_j} = \cupl{j \< n}C_{\pi_j|M_j'}
        \quad\text{для некоторых}\ M_j' \subseteq M_j\ (j \< n).
        \\
    \end{gather*}
\end{lemma}
\begin{proof}
    Докажем по~индукции по~\(n\). Пусть~\(n=1\). Построим множества~\(M_0'\) и~\(M_1'\) такие, что
    \[
        C_{\pi_0|M_0} \cap C_{\pi_1|M_1} = C_{\pi_0|M_0'} \cup C_{\pi_1|M_1'}.
    \]

    Определим
    \begin{align*}
        M_0' &= \l\{k \in M_0 \colon \pi_0(k) \in C_{\pi_1|M_1}\r\},\\
        M_1' &= \l\{k \in M_1 \colon \pi_1(k) \in C_{\pi_0|M_0}\r\}.
    \end{align*}

    Заметим, что для любых~\(s\) и~\(t\) из~\(\N{i}\), \(C_s\) либо содержит~\(C_t\), либо содержится в~\(C_t\) (в частности, они могут быть равны), или эти множества не~пересекаются. Отсюда следует, что
    \[
        C_{\pi_0|M_0'} \cup C_{\pi_1|M_1'} = C_{\pi_0|M_0} \cap C_{\pi_1|M_1}.
    \]

    Далее по~индукции, предположим, что~\(\capl{j \< n}C_{\pi_j|M_j} = \cupl{j \< n}C_{\pi_j|M_j'}\). Тогда
    \[
        \begin{gathered}
            \L(
                \capl{j \< n}C_{\pi_j|M_j}
            \R) \cap C_{\pi_{n+1}|M_{n+1}} =\\
            = \L(
                \cupl{j \< n}C_{\pi_j|M_j'}
            \R) \cap C_{\pi_{n+1}|M_{n+1}}
            = \cupl{j \< n}(C_{\pi_j|M_j'} \cap C_{\pi_{n+1}|M_{n+1}}) =\\
            = \cupl{j \< n}(C_{\pi_j|M_j''} \cup C_{\pi_{n+1}|M_{n+1}^i})
            = \cupl{j \< n+1}C_{\pi_j|M_j''},
        \end{gathered}
    \]
    где~\(M_{n+1}'' = \cupl{j \< n}M_{n+1}^j\).
\end{proof}

\begin{corollary}
    \label{cor:short_base}
    % \label{cor:1.2.1}
    %
    Пусть~\(x \in \SB{1, i}^*\) (\(i = 1, 2, 3\)) и~\(x \in \l[\capl{j \< n}C_{\pi_j|M_j}\r]\), тогда найдутся~\(\pi\) и~\(M \subseteq \omega\) такие, что~\(x \in [C_{\pi|M}] \subseteq \l[\capl{j \< n}C_{\pi_j|M_j}\r]\).
\end{corollary}

Определим
\label{def:gamma_and_q}
\begin{multline*}
    \Gamma_i = \L\{
        C_{\pi|M} \setminus \cupl{\pi \in T'}C_\pi
        \colon \pi \in T_i, T' \subset T, |T'| < \omega, M \subseteq \omega
    \R\}\\
    (i = 1, 2, 3),
\end{multline*}
\[
    \Theta_3 = \L\{
        \N{3} \setminus \L(\cupl{\pi \in T'} C_\pi\R)
        \colon T' \subset T_3, |T'| < \omega
    \R\}.
\]

Теперь по~предложению~\ref{pro:CpiM_in_bool} и~следствию~\ref{cor:short_base} мы~получаем
\begin{theorem}
    \label{the:base}
    % \label{theo:1.2.1}
    %
    Семейства
    \begin{gather*}
        \B[\widetilde]{1, 1} = \l\{[U] \colon U \in \Gamma_1\r\}\quad
        \B[\widetilde]{1, 2} = \l\{[U] \colon U \in \Gamma_2\r\}\\
        \B[\widetilde]{1, 3} = \l\{[U] \colon U \in \Gamma_3 \cup \Theta_3\r\}
    \end{gather*}
    являются базами пространств~\(\SB{1, 1}\), \(\SB{1, 2}\) и~\(\SB{1, 3}\) соответсвенно.
\end{theorem}

Теперь рассмотрим булевы алгебры второго типа на~наших частично упорядоченных множествах~\(\N{1}\), \(\N{2}\) и~\(\N{3}\), порожденные другими семействами множеств.

Обозначим~\(\B{2, i}\) булеву алгебру порожденную семейством множеств
\[
    \{C_s \colon s \in \N{i}\} \cup \{\N{i} \setminus C_s \colon s \in \N{i}\}.
\]

\textls[-15]{Соответсвенно~\(\SB{2, i}\) пространство Стоуна булевой алгебры~\(\B{2, i}\) (\(i = 1, 2, 3\)). Анналогично предложению~\ref{pro:full_base} определяются базы пространств~\(\SB{2, i}\).}
\begin{proposition}
    \label{pro:base_2}
    % \label{prop:1.2.4}
    %
    Семейство
    \[
        \L\{
            \L[
                \L(
                    \capl{s \in N'} C_{s}
                \R) \cap \L(
                    \capl{s \in N''} \N{i} \setminus C_{t_k}
                \R)
            \R] \colon N', N'' \subset \N{i},
                      |N'| < \omega, |N''| < \omega
        \R\}
    \]
    есть база пространства~\(\SB{2, i}\) \textup(\(i = 1, 2, 3\)\textup).
\end{proposition}

Доказательство данного предложения основывается на~предположении~\ref{pro:bool_dissection} и~повторяет доказательство предложения~\ref{pro:full_base}.

\begin{proposition}
    \label{pro:chain_fan_intersection}
    % \label{prop:1.2.5}
    %
    Если~\(\{s_j \colon j \< n\} \subseteq \N{i}\), то~\(\capl{j \< n} C_{s_j} \neq \varnothing\) тогда и~только тогда, когда~\(\{s_j \colon j \< n\}\) является цепью.
\end{proposition}
\begin{proof}
    Непосредственно из~определения~\(C_s\) следует, что~\(C_s \cap C_t\) непусто, тогда и~только тогда, когда~\(s\) и~\(t\) сравнимы (т.е. один является продолжением другого). Отсюда и~следует данное предложение.
\end{proof}

\begin{corollary}
    \label{cor:chain_fan_nesting}
    % \label{cor:1.2.2}
    %
    Если~\(\capl{j \< n} C_{s_j} \neq \varnothing\) для~\(\{s_j \colon j \< n\} \subseteq \N{i}\), то
    \[
        \capl{j \< n} C_{s_j} = C_{s_{j_0}},
    \]
    где~\(\dom\ s_{j_0} = \max \{\dom s_j \colon j \< n\}\).
\end{corollary}

Определим
\begin{gather*}
    \Gamma'_i = \L\{
        C_s \setminus \cupl{t \in N'} C_t
        \colon s \in \N{i}, N' \subset \N{i}, |N'| < \omega
    \R\}\qquad (i = 1, 2, 3),\\
    \Theta'_3 = \L\{
        \N{3} \setminus \cupl{t \in N'} C_{t}
        \colon N' \subset \N{3}, |N'| < \omega
    \R\}.
\end{gather*}

Из~вышеперечисленного вытекает следующая теорема.
\begin{theorem}
    \label{the:base_2}
    % \label{theo:1.2.2}
    %
    Семейства
    \begin{gather*}
        \B[\widetilde]{2, 1} = \l\{[U] \colon U \in \Gamma'_1\r\},\quad
        \B[\widetilde]{2, 2} = \l\{[U] \colon U \in \Gamma'_2\r\},\\
        \B[\widetilde]{2, 3} = \l\{[U] \colon U \in \Gamma'_3 \cup \Theta'_3\r\}
    \end{gather*}
    являются базами пространств~\(\SB{2, 1}\), \(\SB{2, 2}\) и~\(\SB{2, 3}\) соответсвенно.
\end{theorem}
