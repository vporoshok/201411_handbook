%!TEX root = ../main.tex
\subsection{Общие понятия и~факты}
\label{sec:introduction:preliminaries}

Большинство обозначений и~терминов этой работы взяты из~книг Р.\,Энгелькинга~\cite{Engelking:1986}, А.\,В.\,Архангельского и~В.\,И.\,По\-но\-ма\-рё\-ва~\cite{A-P:1974} и~Р.\,Сикорского~\cite{Sikorski:1969}.

Множество всех подмножеств~\(X\) обозначим~\(\exp X\). Через~\(\omega\) обозначается вполне упорядоченное множество~\(\{0, 1, 2, \ldots\}\) неотрицательных целых чисел, а~также мощность этого множества. Множество, имеющее мощность~\(\omega\), называется счётным. Под~\(n\) в~зависимости от~контекста будем понимать и~натуральное число, и~множество~\(\{0, 1, \ldots, n - 1\}\).

Для~множества~\(A \subseteq X\) обозначим:
\begin{description}
    \item[\ensuremath{|A|}] мощность множества~\(A\);
    \item[\ensuremath{[A]}] замыкание множества~\(A\) в~пространстве~\(X\);
    \item[\ensuremath{A^*}] нарост, то~есть~\([A]\setminus A\).
\end{description}

Приведем определения основных кардинальных инвариантов, используемых нами в~работе.
\begin{definition}
    \label{def:base}
    \label{def:weight}
    \index{База}
    %
    \emph{Базой} пространства~\(X\) называется семейство открытых множеств~\(B\) удовлетворяющее следующему условию: для произвольной точки~\(x \in X\) и~произвольной её~окресности~\(Ox\) найдется~\(U \in B\) такое, что~\(x \in U \subseteq Ox\). Наименьшее кардинальное число вида~\(|B|\), где~\(B\) "--- база пространства~\(X\), называется \emph{весом} пространства~\(X\) и~обозначается~\(\weight(X)\).
\end{definition}

\begin{definition}
    \label{def:dense_set}
    \label{def:density}
    \label{def:separable_space}
    \index{Всюду плотное множество}
    \index{Плотность}
    \index{Сепарабельное пространство}
    %
    Подмножество~\(A\) пространства~\(X\) называется \emph{всюду плотным} если~\([A] = X\). Наименьшее кардинальное число вида~\(|A|\), где~\(A\) "--- всюду плотное подмножество пространства~\(X\), называется \emph{плотностью} пространства~\(X\) и~обозначается~\(\density(X)\). Пространства со~счетной плотностью называют \emph{сепарабельными}.
\end{definition}

\begin{definition}
    \label{def:Suslin_number}
    \label{def:Suslin_condition}
    \index{Суслина!число}
    %
    Наименьшее кардинальное число~\(\tau\) такое, что любое семейство попарно непересекающихся непустых открытых подмножеств пространства~\(X\) имеет мощность не~превосходящую~\(\tau\), называется \emph{числом Суслина} пространства~\(X\) и~обозначается~\(\suslin(X)\). Если~\(\suslin(X) = \omega\), то~говорят, что пространство~\(X\) удовлетворяет \emph{условию Суслина}.
\end{definition}

\begin{definition}
    \label{def:tightness}
    \index{Теснота}
    \index{Теснота!в точке}
    %
    \emph{Теснотой в~точке} \(x\) пространства~\(X\) \(\tightness(x, X)\) называется такое наименьшее кардинальное~\(\tau\), что для любого~\(A \subseteq X\), такого что~\(x \in [A]\), найдётся~\(A' \subseteq A\), такое что~\(x \in [A']\) и~\(|A'| \< \tau\). \emph{Теснота} всего пространства определяется как
    \[
        \sup\limits_{x \in X} \tightness(x, X)
    \]
    и~обозначается~\(\tightness(X)\).
\end{definition}
Более подробно о~кардинальных инвариантах написано в~\cite{Archangelskiy:1978}.

\begin{definition}
    \label{def:compact}
    \index{Компактность}
    %
    Пространство называется \emph{компактным} (\emph{бикомпактным}, \emph{компактом}), если из~любого открытого покрытия этого пространства можно выделить конечное подпокрытие.
\end{definition}
\begin{remark}
    Часто мы~будем использовать эквивалентное определение компактности: пространство является компактным тогда и~только тогда, когда пересечение любой центрированной системы замкнутых множеств не~пусто.
\end{remark}
\begin{definition}
    \label{def:compactification}
    \index{Компактификация}
    \index{Бикомпактное расширение|see{Компактификация}}
    %
    Пространство~\(Y\) называется \emph{бикомпактным расширением} или~\emph{компактификацией} пространства~\(X\), если~\(Y\) "--- бикомпакт и~\(X\) гомеоморфно некоторому всюду плотному подмножеству~\(Y\).
\end{definition}

\begin{definition}
    \label{def:Stone--Chech_space}
    \index{Стоуна;--Чеха пространство}
    %
    \emph{Бикомпактным расширением \textup(компактификацией\textup) Стоуна;--Чеха} пространства~\(X\) (обозначаем~\(\beta X\)) будем называть максимальное бикомпактное расширение пространства~\(X\), что означает, что для любого бикомпактного расширения~\(bX\) пространства~\(X\) существует непрерывное отображение~\(f\colon\beta X\to bX\), тождественное на~\(X\).
\end{definition}
\begin{remark}
    Для~нормального пространства~\(X\), его расширение~\(bX\) является пространством Чеха;--Стоуна, если для любых непересекающихся замкнутых множеств~\(F, G \subseteq X\) следует, что~\([F]_{bX} \cap [G]_{bX} = \varnothing\) (доказательство можно найти в~\cite{Engelking:1986}). В~дальнейшем мы~нередко будем использовать этот факт.
\end{remark}

Если в~качестве расширяемого пространства взять~\(\omega\), то~получим пространство~\(\beta\omega\), которое играет особую роль в~теории компактификаций, а~также данной работе, поэтому приведём некоторые его свойства.
\begin{enumerate}
    \item \(\weight(\beta\omega) = \weight(\beta\omega \setminus \omega) = 2^\omega\);
    \item \(\suslin(\beta\omega \setminus \omega) = 2^\omega\);
    \item \(\tightness(\beta\omega) = 2^\omega\);
    \item \(|\beta\omega| = 2^{2^\omega}\).
\end{enumerate}

\begin{definition}
    \label{def:homeomorphizm}
    \index{Гомеоморфизм}
    %
    Непрерывное отображение~\(\varphi \colon X \to Y\) называется \emph{гомеоморфизмом}, если~\(\varphi\) взаимно однозначно отображает~\(X\) на~\(Y\) и~обратное отображение~\(\phi^{-1}\) из~\(Y\) в~\(X\) неперывно. Два~топологических пространства~\(X\) и~\(Y\) называются \emph{гомеоморфными}, если существует гомеоморфизм пространства~\(X\) на~простраснтво~\(Y\).
\end{definition}

В~работе рассматриваются различные частично упорядоченные множества. Введём понятие цепи и~антицепи.
\begin{definition}
    \label{def:chain}
    \index{Цепь}
    %
    \emph{Цепью} в~пространстве называется линейно упорядоченное множество.
\end{definition}

\begin{definition}
    \label{def:antichain}
    \index{Антицепь}
    %
    \emph{Антицепью} в~пространстве называется множество, элементы которого попарно несравнимы.
\end{definition}

\pagebreak

\begin{definition}
    \label{def:bool_algebra}
    \index{Булева алгебра}
    %
    Булевой алгеброй называется непустое множество~\(\mathfrak B\) с~тремя операциями~\(A\cup B\), \(A\cap B\), \(\overline{A}\) удовлетворяющим следующим аксиомам:
    \begin{enumerate}
        \item \(A\cup B=B\cup A\), \(A\cap B=B\cap A\);
        \item \(A\cup(B\cup C)=(A\cup B)\cup C\), \(A\cap(B\cap C)=(A\cap B)\cap C\);
        \item \((A\cap B)\cup B=B\), \((A\cup B)\cap B=B\);
        \item \(A\cap(B\cup C)=(A\cap B)\cup(A\cap C)\), \(A\cup(B\cap C)=(A\cup B)\cap(A\cup C)\);
        \item \((A\cap\overline{A})\cup B=B\), \((A\cup\overline{A})\cap B=B\).
    \end{enumerate}
\end{definition}

Мы~будем рассматривать булевы алгебры~\(\mathfrak B = \{U \subseteq X\}\) в~множестве~\(\exp X\) с~теоретико множественными операциями объединение, пересечение и~дополнение.

На~множестве булевых алгебр из~\(\exp X\) можно ввести отношение порядка по~включению, и~соответственно говорить о~наименьшем элементе. Заметим, что пересечение любого числа алгебр вновь будет алгеброй. Тогда для произвольного множества~\(G \subseteq \exp X\) существует наименьшая алгебра~\(\mathfrak B\), содержащая множество~\(G\). Очевидно~\(\mathfrak B\) может быть определено как пересечение всех алгебр, содержащих множество~\(G\).
\begin{definition}
    \label{def:bool_inherit}
    %
    Будем говорить, что алгебра~\(\mathfrak B\) порождается множеством~\(G\). Если~\(\mathfrak B\) "--- наименьшая алгебра, содержащая множество~\(G\).
\end{definition}

Для~алгебры порожденной множеством справедливо следующее утверждение.
\begin{proposition}
    \label{pro:bool_dissection}
    % \label{prop:1.1.1}
    %
    Если~\(G \subseteq \exp X\) "--- непусто, тогда~\(A\subseteq X\) принадлежит алгебре~\(\mathfrak B\), порожденной~\(G\), в~том и~только в~том случае, когда его можно представить в~виде
    \begin{equation}
        \label{eq:bool_dissection}
        %
        A = (A_{1, 1} \cap \ldots \cap A_{1, r_1})
            \cup \ldots \cup(A_{s, 1} \cap \ldots \cap A_{s, r_s}),
    \end{equation}
    где или~\(A_{m, n} \in G\), или~\(\overline{A}_{m, n} \in G\) для любых~\(m\) и~\(n\).
\end{proposition}
\begin{proof}
    Заметим, что класс элементов вида~\eqref{eq:bool_dissection} замкнут относительно операции объединения. По~формулам Моргана, дополнение к~элементу~\(A\) также может быть представлено в~виде~\eqref{eq:bool_dissection}. Тогда класс элементов вида~\eqref{eq:bool_dissection} замкнут и~относительно опреции дополнения, а~этого достаточно, чтобы соответсвующий класс образовывал алгебру~\(\mathfrak B\), содержащую~\(G\).

    С~другой стороны, каждый элемент~\(A\) вида~\eqref{eq:bool_dissection} принадлежит любой алгебре, содержащей~\(G\). Поэтому~\(\mathfrak B\) является наименьшей алгеброй, содержащей~\(G\).
\end{proof}

Для~определения пространства Стоуна нам необходимо ввести понятие фильтра и~ультрафильтра.
\begin{definition}
    \label{def:filter}
    \index{Фильтр}
    %
    Семейство~\(\xi\) непустых элементов булевой алгебры~\(\mathfrak B\) называется~\emph{фильтром}, если выполнены условия:
    \begin{enumerate}
        \item если~\(A, B \in \xi\), то~\(A \cap B \in \xi\);
        \item если~\(A \in \xi\) и~\(A \subseteq B\), то~\(B \in \xi\).
    \end{enumerate}
\end{definition}

\begin{definition}
    \label{def:uf}
    \index{Ультрафильтр}
    %
    \emph{Ультрафильтром} в~булевой алгебре~\(\mathfrak B\) будем называть такой фильтр~\(\xi\subseteq\mathfrak B\), что он~не~содержится ни~в~одном отличном от~\(\xi\) фильтре в~булевой алгебре~\(\mathfrak B\).
\end{definition}

С~другой стороны, ультрафильтр также можно определить как максимальную центрированную систему.
\begin{definition}
    \label{def:centered_system}
    \index{Центрированная система множеств}
    %
    Система множеств~\(\lambda \subseteq \exp X\) называется \emph{центрированной}, если для любой конечной подсистемы~\(\lambda' \subseteq \lambda\) выполнено~\(\cap\{U \colon U \in \lambda'\}\) не~пусто.
\end{definition}
\begin{definition}
    \label{def:centered_in_family}
    \index{Центрированная система множеств!в семействе}
    %
    Центрированную систему~\(\alpha\) будем называть \emph{центрированной системой в~семействе}~\(\mu\), если~\(\alpha \subseteq \mu\).
\end{definition}

Мы~будем рассматривать прежде всего максимальные центрированные системы различных семейств, состоящих из~элементов булевой алгебры~\(B\). Заметим, что максимальная центрированная система в~семействе подмножеств необязательно замкнута относительно конечных пересечений.
\begin{definition}
    \label{def:centered_sub}
    \index{Центрированная система множеств!вписана}
    %
    Будем говорить, что центрированная система~\(\alpha\) \emph{вписана} в~центрированную систему~\(\alpha'\), если для любого элемента~\(F \in \alpha\) найдётся~\(G \in \alpha'\) такой, что~\(F \subseteq G\).
\end{definition}
\begin{definition}
    \label{def:fix_uf}
    \label{def:free_uf}
    \index{Ультрафильтр!фиксированный}
    \index{Ультрафильтр!свободный}
    %
    Ультрафильтр~\(\xi\) будем называть \emph{фиксированным}, если~\(\cap\{A \colon A \in \xi\} \neq \varnothing\), в~противном случае ультрафильтр называется \emph{свободным}.
\end{definition}

\begin{definition}
    \label{def:coerced_bool}
    \index{Булева алгебра!приведённая}
    %
    Булеву алгебру~\(\mathfrak B \subseteq exp X\) будем называть \emph{приведенной}, если для двух различных точек~\(x, y \in X\) найдется~\(A \in \mathfrak B\) такое, что~\(x \in A\) и~\(y \notin A\).
\end{definition}

Для~фиксированных ультрафильтров справедливо следующее утверждение.
\begin{proposition}
    \label{pro:coerced_bool}
    % \label{prop:1.1.2}
    %
    Если булева алгебра~\(\mathfrak B \subseteq \exp X\) является приведенной. Тогда каждый фиксированный ультрафильтр~\(\xi\) на~\(\mathfrak B\) определяется одной точкой~\(x \in X\) и~\(\cap\{A \colon A \in \xi\} = \{x\}\).
\end{proposition}
\begin{proof}
    Предположим, что найдутся две различные точки~\(x, y \in \cap\{A \colon A \in \xi\}\). В~силу приведенности~\(\mathfrak B\) найдется~\(A_x \in \mathfrak B\) такое, что~\(x \in A_x\) и~\(y \notin A_x\). Каждый элемент~\(\xi\) содержит точку~\(y\), то~есть~\(A_x \notin \xi\). Тогда система~\(\{A \colon A \in \xi\} \cup \{A_x\}\) является фильтром (все её~элементы пересекаются по~точке~\(x\)), а~это притиворечит максимальности~\(\xi\).
\end{proof}

В~дальнейшем мы~будем рассматривать только приведенные булевы алгебры.

Фиксированный ультрафильтр в~точке~\(x \in X\) будем обозначать, как~\(\widehat{x}\), а~множество всех фиксированных ультрафильтров обозначим~\(\widehat{X}\).
\begin{definition}
    \label{def:SB}
    \index{Стоуна пространство}
    %
    \textls[-5]{Пространством Стоуна~\(S\mathfrak B\) булевой алгебры~\(\mathfrak B\) называется множество ультрафильтров в~\(\mathfrak B\) с~топологией, задаваемой базой состоящей их~открыто"=замкнутых подмножеств~\([A]\) следующего вида:}
    \[
        [A] = \{\xi \in S\mathfrak B \colon A \in \xi\},\quad\text{для}\quad A \in \mathfrak B.
    \]
\end{definition}

Множество свободных ультрафильтров из~\([A]\) будем обозначать~\(A^* = [A] \setminus \widehat{A}\). Множество всех свободных ультрафильтров будем обозначать~\(S\mathfrak B^*\).
\begin{definition}
    \label{def:uf_basis}
    \index{Ультрафильтр!базис}
    %
    \emph{Базисом} ультрафильтра~\(\xi\) называется подсемейство~\(\sigma \subseteq \xi\) удовлетворяющее следующему условию: для произвольного~\(F \in \xi\) найдется~\(F' \in \sigma\) такое, что~\(F' \subseteq F\).
\end{definition}

Отметим, что в~пространстве Стоуна если~\(\sigma\) базис ультрафильтра~\(\xi\), то~семейство~\(\widetilde{\sigma} = \{[F] \colon F \in \sigma\}\) является базой в~точке~\(\xi\), как точке пространства Стоуна. То~есть, для любой окрестности~\(U\) точки~\(\xi\) найдётся~\(F \in \sigma\) такое, что~\(\xi \in [F] \subseteq U\).
\begin{theorem}[\cite{Sikorski:1969}]
    \label{the:SB_is_compact}
    % \label{theo:1.1.1}
    %
    Пространство Стоуна~\(S\mathfrak B\) является бикомпактным.
\end{theorem}
\begin{proof}
    Пусть~\(S\mathfrak B\) пространство Стоуна некоторой булевой алгебры~\(\mathfrak B\). Рассмотрим центрированную систему замкнутых множеств~\(\{F_r \colon r \in R\}\) в~пространстве~\(S\mathfrak B\).

    Тогда по~определению топологии~\(S\mathfrak B\) для каждого~\(r \in R\) найдется набор~\(\{A_q \colon A_q \in \mathfrak B, q \in Q_r\}\) такой,что~\(F_r = \capl{q \in Q_r}[A_q]\). Получим центрированную систему множеств
    \[
        \{[A_q] \colon q \in Q\},
        \quad\text{где}\quad
        Q = \cupl{r \in R} Q_r.
    \]

    Но~тогда центрированной является и~система~\(\{A_q\colon q\in Q\}\subseteq\mathfrak B\), а~значит её~можно дополнить до~некоторого ультрафильтра
    \[
        \xi \in \capl{q \in Q}[A_q] = \capl{r \in R}F_r.
    \]
\end{proof}

Докажем некоторые свойства пространства Стоуна~\(S\mathfrak B\) произвольной булевой алгебры~\(\mathfrak B \subseteq \exp X\), необходимые в~дальнейшем.
\begin{proposition}
    \label{pro:SB_cap_of_closures}
    % \label{prop:1.1.3}
    %
    Если~\(U, V \subseteq X\), \(V\) "--- элемент булевой алгебры и~\(U \cap V=\varnothing\), тогда~\([U] \cap [V] = \varnothing\).
\end{proposition}

\begin{proposition}
    \label{pro:SB_closure_of_cap}
    % \label{prop:1.1.4}
    %
    Если~\(U, V \subseteq X\) и~\(V\) "--- элемент булевой алгебры, тогда~\([U \cap V] = [U] \cap [V]\).
\end{proposition}
\begin{proof}
    Действительно, \(U = (U \cap V) \cup (U \setminus V)\) тогда, учитывая предложение~\ref{pro:SB_cap_of_closures},
    \begin{gather*}
        [U] \cap [V]
            = [(U \cap V) \cup (U \setminus V)] \cap [V] =\\
            = ([U \cap V] \cup [U \setminus V]) \cap [V] =\\
            = ([U \cap V] \cap [V]) \cup ([U \setminus V] \cap [V])
            = [U \cap V].
    \end{gather*}
\end{proof}

\begin{proposition}
    \label{pro:SB_clopen_repr}
    % \label{prop:1.1.5}
    %
    Для~любого открыто-замкнутого~\(U \subseteq S\mathfrak B^*\) найдётся~\(V \subseteq X\) такое, что~\([V] \cap S\mathfrak B^* = U\).
\end{proposition}
\begin{proof}
    Так~как~\(U\) открытое множество, то~для каждой точки~\(x \in U\) найдётся окрестность~\(Ox = [V_x]\)~(\(V_x \in \mathfrak B\)) такая, что~\(Ox \cap S\mathfrak B^* \subseteq U\).

    Таким образом, семейство
    \[
        \lambda = \l\{Ox\cap S\mathfrak B^* \colon x \in U\r\}
    \]
    является открытым покрытием множества~\(U\).

    С~другой стороны, так как~\(U\) замкнутое подмножество бикомпактного пространства~\(S\mathfrak B^*\), можно выделить~\(\lambda'\) конечное подпокрытие~\(\lambda\)
    \[
        \lambda' = \l\{O{x_i} \cap S\mathfrak B^* \colon i \< n\r\}.
    \]
    То~есть
    \[
        U = \cup \l\{[V_{x_i}] \colon i \< n\r\} \cap S\mathfrak B^*
            = \l[\cup \l\{V_{x_i} \colon i \< n\r\}\r] \cap S\mathfrak B^*.
    \]
    Множество~\(V = \cup \l\{V_{x_i} \colon i \< n\r\}\) есть элемент~\(\mathfrak B\) и~\(U = [V] \cap S\mathfrak B^*\).
\end{proof}

Заметим, что максимальное бикомпактное расширение Стоуна;--Чеха дискретного пространства~\(X\) можно рассматривать, как пространства Стоуна булевой алгебры множества всех подмножеств пространства~\(X\). А~одноточечное расширение Александрова бесконечного дискретного пространства~\(X\) рассматривать, как пространство Стоуна булевой алгебры порожденной семейством конечных подмножеств пространства~\(X\).
