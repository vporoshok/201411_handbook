%!TEX root = ../main.tex
\subsection[Пространство \mathpdf{\SB{1, 3}}{SB(1, 3)}]{Пространство~\(\SB{1, 3}\)}
\label{sec:all_in:SB_1_3}

Данное пространство отличается от~двух рассмотренных ранее тем, что множество фиксированных ультрафильтров~\(\N[\widehat]{3}\) не~является его дискретным подпространством.

\begin{lemma}
    \label{lem:2.3.0}
    %
    Множество свободных ультрафильтров~\(\SB{1, 3}^*\) всюду плотно в~\(\SB{1, 3}\).
\end{lemma}
\begin{proof}
Рассмотрим произвольное~\(U \in \Gamma_3 \cup \Theta_3\) (\(\Gamma_i\) и~\(Q_3\) определены на странице~\pageref{def:gamma_and_q}). Легко построить бесконечную цепь~\(\{s_n \colon n \in \omega\} \subseteq U\). Для~этого достаточно заметить, что для произвольного~\(s \in U\) количество его продолжений на~следующий шаг бесконечно, а~по~определению~\(\Gamma_3\)и~\(\Theta_3\), только конечное их~число может не~содержаться в~\(U\).

Для~каждого~\(k \in \omega\) определим~\(A_k = \{s_n \colon n \> k\}\). Тогда для всякого~\(A_k\) (\(k \in \omega\)) выполнены следующие свойства:
\begin{enumerate}
    \item \([A_k] \cap \N[\widehat]{3} = A_k\);
    \item \([A_k] \subseteq U\).
\end{enumerate}

Получаем центрированную систему, состоящую из~замкнутых множеств~\(\{[A_k] \colon k \in \omega\}\) и, в~силу бикомпактности~\(\SB{1, 3}\), \(\capl{k \in \omega} [A_k] \neq \varnothing\), при этом~\(\capl{k \in \omega} [A_k] \subseteq \SB{1, 3}^*\).
\end{proof}

Выделим несколько типов точек данного пространства с~базами определенного вида.

\begin{lemma}
    \label{lem:2.3.1}
    %
    \(\Theta_{1, 3} = \{\N{3} \setminus (\cupl{\pi \in T'} C_\pi) \colon T' \subset T_3, |T'| < \omega\}\) является базисом некоторого ультрафильтра~\(\xi_0\).
\end{lemma}
\begin{proof}
    Семейство~\(\Theta_{1, 3}\) центрировано.

    Пусть~\(\xi_0 \in \SB{1, 3}\) "--- ультрафильтр, мажорирующий~\(\Theta_{1, 3}\). Никакое множество из~\(\Gamma_{1, 3}\) вида~\(C_{\pi|M} \setminus \cupl{\pi \in T'} C_{\pi}\) (\(T' \subset T_3\), \(|T'| < \omega\)) ультрафильтру~\(\xi_0\) не~принадлежит, поскольку~\(\N{3} \setminus C_{\pi|M} \in \Theta_{1, 3}\). Таким образом, \(\Theta_{1, 3}\) "--- базис ультрафильтра~\(\xi_0\).
\end{proof}

\begin{lemma}
    \label{lem:2.3.2}
    %
    Пусть~\(s\in\N{3}\). Семейство множеств
    \[
        \sigma_s = \{
            C_s \setminus \cupl{\pi \in T'} C_{\pi} \colon
            T' \subseteq T_3, |T'| < \omega,
            s \notin \cupl{\pi \in T'} C_{\pi}
        \}
    \]
    является базисом фиксированного по~\(s\) ультрафильтра~\(\widehat{s}\).
\end{lemma}
\begin{proof}
    Во-первых, отметим, что всякое множество семейства~\(\sigma_s\) содержит~\(s\) и~является элементом булевой алгебры~\(\B{1, 3}\), следовательно, принадлежит ультрафильтру~\(\widehat{s}\).

    Если множество~\(C_{\pi|M} \setminus \cupl{\pi \in T'} C_{\pi} \in \widehat{s}\) для некоторых~\(\pi \in T_3\), \(T' \subseteq T_3\), \(|T'| < \omega\), \(M \subseteq \omega\), то~\(C_{\pi|M} \in \widehat{s}\), а~это значит, что~\(C_{\pi(n)} \ni s\) для некоторого~\(n \in M\). Таким образом, \(C_{\pi(n)} \setminus \cupl{\pi \in T'} C_{\pi} \in \sigma_s\) и~\(C_{\pi(n)} \setminus \cupl{\pi \in T'} C_{\pi} \subseteq C_{\pi|M} \setminus \cupl{\pi \in T'} C_{\pi}\)
\end{proof}

\begin{lemma}
    \label{lem:2.3.3}
    %
    Пусть~\(\alpha = \{s_n \colon n \in \omega\}\) "--- бесконечная цепь в~\(\N{3}\). Тогда семейство непустых множеств
    \[
        \sigma_\alpha = \{
            C_s \setminus \cupl{\pi \in T'} C_{\pi} \colon
            s \in \alpha, T' \subseteq T_3, |T'| < \omega,
            \alpha \cap (\cupl{\pi \in T'} C_{\pi}) = \varnothing
        \}
    \]
    является базисом свободного ультрафильтра~\(\xi_\alpha\).
\end{lemma}
\begin{proof}
    Из~определения семейства~\(\sigma_\alpha\) следует, что семейство~\(\sigma_\alpha\) центрировано. Дополним~\(\sigma_\alpha\) до~ультрафильтра, обозначим его~\(\xi_\alpha\). Покажем, что~\(\sigma_\alpha\) является базисом этого ультрафильтра.

    Пусть~\(C_{\pi|M} \setminus \cupl{\pi \in T'} C_{\pi} \in \xi_\alpha\) для некоторых~\(\pi \in T_3\), \(T' \subseteq T_3\), \(|T'| < \omega\) и~\(M \subseteq \omega\). Тогда найдутся~\(s_n \in \alpha\) и~\(m \in M\) такие, что~\(C_{\pi(m)} \ni s_n\). Действительно, в~противном случае~\(\alpha \cap C_{\pi|M} = \varnothing\) и~найдется~\(\pi' \in T_3\) такое, что~\(\pi'|M = \pi|M\) и~\(\alpha \cap C_{\pi'} = \varnothing\).

    Тогда для~\(s_n \in \alpha\) имеем~\(U = C_{s_n} \setminus ((\cupl{\pi \in T'} C_{\pi}) \cup C_{\pi'}) \in \sigma_\alpha\) и~следовательно~\(U \in \xi_\alpha\). С~другой стороны, поскольку~\(C_{\pi'} \supseteq C_{\pi|M}\), имеем~\(U \cap (C_{\pi|M} \setminus \cupl{\pi \in T'} C_{\pi}) = \varnothing\), что противоречит центрированности~\(\xi_\alpha\) как ультрафильтра.

    Поскольку~\(\alpha\) "--- бесконечная цепь, то~\(\xi_\alpha\) является свободным ультрафильтром. При~этом для всякой бесконечной цепи~\(\alpha' \subseteq \alpha\) имеем~\(\xi_{\alpha'} = \xi_\alpha\).
\end{proof}

Множество свободных ультрафильтров~\(\xi_\alpha\), построенных по~бесконечным цепям~\(\alpha \subseteq \N{3}\), будем обозначать~\(\widehat{F}_1\), а~сами ультрафильтры~\(\xi_\alpha\) будем называть ультрафильтрами первого рода.

\begin{lemma}
    \label{lem:2.3.4}
    %
    Если~\(\xi \in \SB{1, 3}^* \setminus \widehat{F}_1 \cup \{\xi_0\}\), то~найдется множество~\(C_{\pi|M}\) такое, что~\(\pi \in T_3\), \(M \subseteq \omega\), \(|M| = \omega\) и~\(\{\pi(n) \colon n \in M\}\) есть строгая антицепь и
    \[
        \xi \ni C_{\pi|M}
        \ \text{и}
        \ C_{\pi(n)} \notin \xi
        \ \text{для всякого}
        \ n \in M.
    \]
\end{lemma}
\begin{proof}
    Предположим, что для всякого~\(C_{\pi|M}\) такого, что~\(C_{\pi|M} \in \xi\) найдется~\(n \in M\) такое, что~\(C_{\pi(n)} \in \xi\).

    Рассмотрим семейство~\(\lambda = \{s \in \N{3} \colon C_s \in \xi\}\). Тогда~\(\lambda\) "--- цепь. В~силу нашего предположения, у~ультрафильтра~\(\xi\) есть базис~\(\sigma = \{C_s \setminus \cupl{\pi \in T'} C_{\pi} \colon \lambda \cap (\cupl{\pi \in T'} C_{\pi}) = \varnothing, T' \subseteq T_3, |T'| < \omega\}\). Тогда~\(\xi\) есть или фиксированный ультрафильтр, или свободный ультрафильтр 1 рода, что противоречит условию леммы.
\end{proof}

Ультрафильтры из~множества~\(\widehat{F}_2 = \SB{1, 3}^* \setminus (\widehat{F}_1 \cup \{\xi_0\})\) будем называть свободными ультрафильтрами второго рода.

Из~лемм~\ref{lem:2.3.1},~\ref{lem:2.3.2},~\ref{lem:2.3.3},~\ref{lem:2.3.4} следует
\begin{theorem}
    \label{theo:2.3.1}
    %
    \(\SB{1, 3} = \{\xi_0\} \cup \N[\widehat]{3} \cup \widehat{F}_1\cup \widehat{F}_2\).
\end{theorem}

\begin{lemma}
    \label{lem:2.3.5}
    %
    Пусть~\(\xi \in \widehat{F}_1 \cup \widehat{F}_2\) свободный ультрафильтр. Тогда существует~\(C_{\pi|M} \in \B{1, 3}\) такое, что
    \begin{enumerate}
        \item \(|M| = \omega\);
        \item \(\xi \ni C_{\pi|M}\);
        \item если~\(\{M_k \colon k \< k_0\}\) "--- конечное разбиение~\(M\), то~найдется~\(k'\), \(k' \< k_0\) такое, что~\(|M_{k'}| = \omega\) и~\(\xi \ni C_{\pi|M_{k'}}\).
    \end{enumerate}
\end{lemma}
\begin{proof}
    Если~\(\xi \in \widehat{F}_1\) "--- свободный ультрафильтр 1 рода, порожденный полной цепью~\(\alpha = \{s_n \colon n \in \omega\}\), то~определим~\(\pi \in T_3\) по~правилу: \(\pi(n) = s_n\). Положим~\(M = \omega\). Тогда~\(C_\pi\) искомое множество.

    Если~\(\xi \in \widehat{F}_2\) "--- свободный ультрафильтр 2 рода, то~по~лемме~\ref{lem:2.3.4} существует~\(C_{\pi|M} \in \B{1, 3}\) такое, что~\(|M| = \omega\), \(\{\pi(n) \colon n \in M\}\) есть строгая антицепь и~\(\xi \in [C_{\pi|M}] \setminus \cup \{[C_{\pi(n)}] \colon n \in M\}\). Тогда~\(C_{\pi|M}\) искомое.
\end{proof}

\begin{theorem}
    \label{theo:2.3.2}
    %
    \(\suslin(\SB{1, 3}^*) = \omega\).
\end{theorem}
\begin{proof}
    Рассмотрим произвольную дизъюнктную систему открытых множеств~\(\nu = \{U_\alpha \colon \alpha \in A\}\) в~\(\SB{1, 3}^*\). По~теореме~\ref{the:base}, найдется система множеств~\(\nu' = \{V_\alpha \colon \alpha \in A\}\) такая, что~\(V_\alpha \in \Gamma_{1, 3} \cup \Theta_{1, 3}\) и~\([V_\alpha] \cap \SB{1, 3}^* \subseteq U_\alpha\) для всех~\(\alpha \in A\).

    Покажем, что~\(\nu'\) дизъюнктно. Предположим противное. Тогда найдутся~\(\alpha, \beta \in A\) такие, что~\(V_\alpha \cap V_\beta \neq \varnothing\). Заметим, \(V_\alpha \cap V_\beta \in \B{1, 3}\), а~значит~\([V_\alpha \cap V_\beta]\) открыто-замкнутое множество, и~по~лемме~\ref{lem:2.3.0} справедливо~\([V_\alpha \cap V_\beta] \cap \SB{1, 3}^* \neq \varnothing\).

    Получаем~\(\varnothing \neq [V_\alpha \cap V_\beta] \cap \SB{1, 3}^* \subseteq [V_\alpha] \cap [V_\beta] \cap \SB{1, 3}^* \subseteq U_\alpha \cap U_\beta\), что противоречит дизъюнктности~\(\nu\).

    Предположим, что~\(|A| > \omega\). Тогда из~дизъюнктности~\(\nu\) следует существование на~счетном~\(\N{3}\) несчетной системы дизъюнктных множеств, чего быть не~может.
\end{proof}

\begin{theorem}
    \label{theo:2.3.3}
    %
    Подпространство свободных ультрафильтров пространства~\(\SB{1, 3}^*\) не~сепарабельно.
\end{theorem}
\begin{proof}
    Пусть~\(A = \{\theta_n \colon n \in \omega\}\) "--- счетное подмножество~\(\SB{1, 3}^* \setminus \{\xi_0\}\). Мы~покажем, что существует строгая антицепь~\(\{s_n \colon n \in \omega\}\) такая, что~\(A \subseteq [\cup \{C_{s_n} \colon n \in \omega\}]\).

    Рассмотрим произвольное~\(n \in \omega\) и~\(\xi_n \in A\). Обозначим~\(C_{\pi_n|M_n}\) множество булевой алгебры~\(\B{1, 3}\) удовлетворяющее условиям леммы~\ref{lem:2.3.5}.

    Тогда существует число~\(m_n \in \{0, \ldots, 10^{n + 1} - 1\}\) такое, что для класса вычетов~\(\overline{m}_n\) по~модулю~\(10^{n + 1}\), определяемого числом~\(m_n\), выполняется~\(C_{\pi_n|\overline{m}_n \cap M} \in \xi\). Класс вычетов~\(\overline{m}_n\) состоит из~точек вида~\(p^r_n = 10^{n + 1} \cdot r + m_n\), где~\(r \in \omega\), \(n \in \omega\). Обозначим~\(\overline{m}_n' = \{p^r_n \in \overline{m}_n \colon r \> 1\}\).

    Для~любых чисел~\(p^r_n, p^{r - 1}_n \in \overline{m}_n'\) (\(r \> 1\)) мы~определим число~\(k^r_n\) и~\(t^r_n \in \N{3}\) такие, что выполнены следующие условия:
    \begin{enumerate}
        \item
            \label{theo:2.3.3:1}
            %
            \(p^{r - 1}_n \< k^r_n < p^r_n\);
        \item
            \label{theo:2.3.3:2}
            %
            \(\dom t^r_n = k^r_n + 1\);
        \item
            \label{theo:2.3.3:3}
            %
            \(\pi_n(p^r_n) \> t^r_n\);
        \item
            \label{theo:2.3.3:4}
            %
            \(k^r_n \neq k^{r'}_{n'}\) если не~выполнено~\(n = n'\) и~\(r = r'\).
    \end{enumerate}

    Мы~будем обозначать~\(\widetilde{M}_n = \{k^r_n \colon r \> 1\}\). Построение множеств~\(\{\widetilde{M}_n \colon n \in \omega\}\) будем осуществлять по~индукции.

    Для~\(n = 0\) положим~\(\widetilde{M}_0 =\{\overline{m}_0'\}\).

    Пусть построены множества~\(\widetilde{M}_i\) для~\(i < n\). Определим множество~\(\widetilde{M}_n\). Для~этого рассмотрим произвольные~\(r \> 1\) и~\(p^r_n \in \overline{m}_n'\), \textls[-20]{то~есть~\(p^r_n\!=\!10^{n + 1} \cdot r + m_n\). Рассмотрим также~\(p^{r - 1}_n\!=\!10^{n + 1} \cdot (r - 1) + m_n\).} Обозначим~\(I = [10^{n + 1} \cdot (r - 1) + m_n, 10^{n + 1} \cdot r + m_n]\) отрезок натуральных чисел. Нетрудно видеть, что~\(|I \cap (\cup \{\widetilde{M}_i \colon i < n\})| < 10^{n + 1}\). Тогда найдется число~\(k^r_n\!\in\!I \setminus \cup \{\widetilde{M}_i\!\colon i\!<\!n\}\). Обозначим~\(\widetilde{M}_n\!=\!\{k^r_n\!\colon r\!\>\!1\}\).

    Для~чисел~\(k^r_n\) выберем и~зафиксируем элемент~\(t^r_n \in \N{3}\) такой, что
    \[
        t^r_n \< \pi_n(p^r_n)
        \quad\text{и}\quad
        \dom t^r_n = k^r_n + 1.
    \]

    Тогда для множеств~\(\{\widetilde{M}_i \colon i \< n\}\) и~\(\{t^r_i \colon i \< n, r \> 1\}\) выполнены условия~\ref{theo:2.3.3:1}--\ref{theo:2.3.3:4}.

    Таким образом, построены множество~\(\widetilde{M} = \cupl{n \in \omega} \widetilde{M}_n\) и~множество~\(\{t(k^r_n) = t^r_n \colon k^r_n \in \widetilde{M}\}\).


    В~силу условия~\ref{theo:2.3.3:4} множество~\(\cup \{C_{t(k^r_n)} \colon k^r_n \in \widetilde{M}\}\) есть элемент булевой алгебры~\(\B{1, 3}\).

    В~силу условия~\ref{theo:2.3.3:3} мы~имеем
    \[
        \theta_n \ni \cup \{
            C_{t(k^r_n)} \colon
            k^r_n \in \widetilde{M}
        \}\ \text{для всех}\ n \in \omega,
    \]
    и, следовательно,~\(\{\theta_n \colon n \in \omega\}\) лежит в~открыто-замкнутом множестве~\([\cup \{C_{t(k^r_n)} \colon k^r_n \in \widetilde{M}\}]\). При~этом~\(\SB{1, 3} \setminus [\cup \{C_{t(k^r_n)} \colon k^r_n \in \widetilde{M}\}] \neq \varnothing\).
\end{proof}
