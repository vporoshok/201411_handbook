#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

config = {
    # 'ignored_env': [
    #     'blockquote',
    #     'annotation',
    #     'theorem',
    #     'enumerate',
    #     'itemize',
    #     'description',
    #     'definition',
    #     'lemma',
    #     'proof',
    #     'example',
    #     'remark'
    # ],
    'l_break_cmd': [
        'phantomsection',
        'markboth',
        'addcontentsline'
    ],
    #     'begin',
    #     'end',
    #     'item',
    #     'author',
    #     'title',
    #     'date',
    #     'label',
    #     'index',
    #     'chapter',
    #     'section',
    #     'subsection',
    #     'subsubsectipn',
    #     '[',
    #     ']'
    # ],
    'r_break_cmd': [
        'phantomsection',
        'markboth',
        'addcontentsline'
    ],
    #     'begin',
    #     'end',
    #     'author',
    #     'title',
    #     'date',
    #     'label',
    #     'index',
    #     'chapter',
    #     'section',
    #     'subsection',
    #     'subsubsectipn',
    #     '[',
    #     ']'
    # ],
    # 'remove_comments': False,
    # 'rules': [
    #     (r'([^ ]) +', r'\1 '),
    #     (r'\t', r'    '),
    #     # Год
    #     (r'([^\d])(\d{2}|\d{4})[~ ]г.', r'\1\2\,г.'),
    #     # Город
    #     (r'([^,])г.[~ ]([А-ЯЁ][а-яё]+)', r'\1г.\,\2'),
    #     # Инициалы
    #     (r'([А-ЯЁ]\.)[~ ]?([А-ЯЁ]\.)[~ ]?([А-ЯЁ][а-яё]+)', r'\1\,\2\,\3'),
    #     (r'([А-ЯЁ][а-яё]+)[~ ]?([А-ЯЁ]\.)[~ ]?([А-ЯЁ]\.)', r'\1~\2\,\3'),
    #     # Формулы
    #     (r'([А-ЯЁа-яё]) \\\(', r'\1~\('),
    #     (r'\\\)-', r'\);-'),
    #     # Тире
    #     (r'([а-яё])(\\,|[~ ])?--(\\,|[~ ])?([А-ЯЁ])', r'\1;--\4'),
    #     (r'(\d)(\\,|[~ ])?--(\\,|[~ ])?(\d)', r'\1--\4'),
    #     (r' --[~ ]', r' "--- '),
    #     (r' ---[~ ]', r' "--- '),
    #     # Короткие слова
    #     (r'(\\,|[~ ]|^)(?!б|бы|же|ли)([А-ЯЁ]?[а-яё]{1,2}) ', r'\1\2~', 2),
    #     (r' (б|бы|же|ли)(\\,|[~ ])', r'~\1\2', 2),
    #     # Команды счётчиков
    #     (r' (\\(cite|ref|upref))', r'~\1')
    # ]
}
