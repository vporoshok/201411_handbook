%!TEX root = ../main.tex
\subsection[Конструкция, свойства и~базы расширения Белла]{\textls[-5]{Конструкция, свойства и~базы расширения Белла}}
\label{sec:Bell_space:originals}

Рассмотрим построение расширения Белла~\cite{Bell:1980}.

Определим множество функций
\[
    P = \l\{
        f \in \omega^\omega
        \colon 0 \< f(n) \< n+1\ \text{для всех}\ n \in \omega
    \r\}.
\]
В~качестве счётного дискретного пространства~\(N\) возьмём множество всех сужений функций множества~\(P\):
\[
    N = \l\{
        f|_n
        \colon f \in P, n \subseteq \omega
    \r\}.
\]

Определим множество
\[
    T = \l\{
        \pi \in N^\omega
        \colon \dom \pi(n) = n + 1\ \text{для всех}\ n \in \omega
    \r\}.
\]

Для~каждой точки~\(s \in N\) пусть~\(C_s = \{t \in N \colon t|_{\dom s} = s\}\). А~для каждого~\(\pi \in T\) обозначим
\[
    C_\pi = \cup \l\{C_{\pi(n)} \colon n \in \omega\r\}.
\]

Обозначим через~\(B\) булеву алгебру, порождённую семейством
\[
    B' = \l\{
        C_\pi \colon \pi \in T
    \r\} \cup \l\{
        N \setminus C_\pi \colon \pi \in T
    \r\}.
\]
Положим~\(B'' = \{U \in B \colon |U| = \omega\r\}\).

Определим пространство~\(BN\) как пространство Стоуна булевой алгебры~\(B\).

Пусть~\(n \in \omega\), \(s \in N\) такие, что~\(\dom s \< n\). Определим
\begin{multline*}
    \Gamma(s) = \L\{
        C_{\pi|M} \setminus \cupl{i \< m} C_{\pi_i} \colon
        \pi(\min M) = s, s \notin \cupl{i \< m} C_{\pi_i},\\
        (m+1)(n+1) < \dom s+1
    \R\}.
\end{multline*}

М.\,Беллом было показано~\cite{Bell:1980}
\begin{theorem}
    \label{teo:Bell}
    %
    Семейство~\(B''\) представимо в~виде счётного объединения~\(2\);-сцепленных семейств.
\end{theorem}
Из~этой теоремы непосредственно вытекает
\begin{corollary}
    Пространство~\(BN \setminus N\) удовлетворяет условию Суслина, но~несепарабельно.
\end{corollary}

А.\,Грызловым в~\cite{Gryzlov:1996} был доказано следующее свойство пространства Белла.
\begin{theorem}
    \label{teo:Gryzlov}
    %
    Для~любого натурального~\(m\) существует система множеств~\(B_m\subseteq B''\) такая, что
    \begin{enumerate}[\upshape 1.]
        \item семейство дополнений до~элементов системы~\(B_m\) "--- \(m\);-сцеплено;
        \item для любого счётного множества точек~\(\{p_k \colon k \in \omega\} \subseteq BN \setminus N\) найдётся множество~\(U \in B_m\) такое, что~\(\{p_k \colon k \in \omega\} \subseteq [U]\).
    \end{enumerate}
\end{theorem}

Следующая теорема объединяет свойства, доказанные М.\,Бел\-лом и~А.\,Грызловым.
\begin{theorem}
    \label{teo:BellGry}
    %
    ~
    \begin{enumerate}[\upshape 1.]
        \item
            \label{teo:BellGry:1}
            Пусть~\(n \in \omega\) и~\(s \in N\) такие, что~\(\dom s \> n\). Тогда для~\(\Gamma(s)\) следующее верно:
            \begin{enumerate}[\upshape a)]
                \item
                    \label{teo:BellGry:1.a}
                    \(\Gamma(s)\) "--- \(n\);-сцепленно;
                \item
                    \label{teo:BellGry:1.b}
                    семейство дополнений до~элементов~\(\Gamma(s)\) "--- \(n\);-сцепленно;
                \item
                    \label{teo:BellGry:1.c}
                    \textls[-12]{для любого счётного множества точек~\(\{p_i \colon i \in \omega\} \subseteq BN \setminus N\) найдётся множество~\(U \in \Gamma(s)\) такое, что~\(\{p_i \colon i \in \omega\} \subseteq [U]\).}
            \end{enumerate}
        \item
            \label{teo:BellGry:2}
            Для~всякого~\(n \in \omega\) семейство
            \[
                \Gamma_n = \l\{
                    [U] \setminus N \colon
                    U \in \cup \l\{
                        \Gamma(s) \colon
                        \dom s \> n
                    \r\}
                \r\}
            \]
            является базой пространства~\(BN \setminus N\).
    \end{enumerate}
\end{theorem}
\begin{proof}
    \textbf{\ref{teo:BellGry:1.a}}\quad Доказательство этого пункта является модификацией доказательство Белла (см.~\cite{Bell:1980}). Пусть~\(n \in \omega\) и~\(s \in N\) такие, что~\(\dom s \> n\). Пусть~\(U_0, \ldots, U_n \in \Gamma(s)\). Тогда
    \[
        s \in U_j = C_{\pi_j|M_j} \setminus \cupl{i \< {m_j}} C_{\pi_i^j}
        \qquad(j \< n).
    \]
    Мы~построим~\(h \in P\) такое, что~\(\l\{h|_k \colon k \> \dom s\} \subseteq U_0 \cap \ldots \cap U_n\), по~индукции по~\(k \> \dom s\). Пусть~\(h|_{\dom s} = s \in U_0 \cap \ldots \cap U_n\). Если~\(h|_k \in U_0 \cap \ldots \cap U_n\) для~\(k \> \dom s\) определено, мы~можем определить~\(h|_{k+1} \notin \{\pi_i^j(k) \colon i \< m_j, j \< n\}\) поскольку
    \[
        \l|
            \l\{
                (i, j) \colon j \< n, i \< m_j
            \r\}
        \r| \< (n + 1) \cdot (\max m_j + 1) < k + 2,
    \]
    и~существует~\(k + 2\) продолжения~\(h|_k\) на~\(k + 1\).

    \textbf{\ref{teo:BellGry:1.b}}\quad Пусть~\(U_j = C_{\pi_j|M_j} \setminus \cupl{i \< {m_j}} C_{\pi_i^j} \in \Gamma(s)\)~(\(j \< n\)). Построим~\(h \in P\) такое, что~\(\{h|_k \colon k \> \dom s\} \subseteq N \setminus \cupl{j \< n} C_{\pi_j|M_j}\).

    Пусть~\(h|_{\dom s} \neq s\), тогда~\(h|_{\dom s} \notin \cupl{j \< n} C_{\pi_j|M_j}\).

    Если~\(h|_k \notin \cupl{j \< n} C_{\pi_j|M_j}\) для~\(k \> \dom s\) определено, мы~можем определить~\(h|_{k+1} \notin \cupl{j \< n} C_{\pi_j|M_j}\), поскольку
    \[
        \l|
            \l\{
                \pi_j(k) \colon j \< n
            \r\}
        \r| \< n+1 \< \dom s < k + 2,
    \]
    и~существует~\(k+2\) продолжения~\(h|_k\) на~\(k+1\).

    Легко увидеть, что~\(\Gamma(s)\) удовлетворяет условию~\textbf{\ref{teo:BellGry:1.c}}.

    \textbf{\ref{teo:BellGry:2}}\quad Пусть точка~\(x \in BN \setminus N\), \(Ox = C_{\pi|M} \setminus \cupl{i \< m} C_{\pi_i}\) и~\(\ell_0\) такое, что~\((m + 1)(n + 1) < \ell_0 - 1\) и~\(\ell_0 \> n\). Существует~\(s_0\) такое, что~\(\dom s_0 = \ell_0\) и~\(x \in [C_{s_0}]\). Мы~имеем
    \[
        C_{s_0} \cap \L(
            C_{\pi|M} \setminus \cupl{i \< m} C_{\pi_i}
        \R) = C_{\pi|M'} \setminus \cupl{i \< m} C_{\pi_i},
    \]
    где~\(M' = \{\ell \colon \ell \in M\ \text{и}\ \pi(\ell)\ \text{продолжение}\ s_0\}\).

    Таким образом~\(\min M' \> \ell_0\), тогда~\((m + 1)(n + 1) < \min M' + 1\) и~\(\min M' \> n\). В~итоге, для~\(s = \pi(\min M')\) мы~имеем
    \[
        U = C_{\pi|M'} \setminus \cupl{i \< m} C_{\pi_i}
            \in \Gamma(s).
    \]
    Итак, \(U\) "--- искомая окрестность.
\end{proof}
