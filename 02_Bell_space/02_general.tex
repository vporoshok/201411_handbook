%!TEX root = ../main.tex
\subsection[Замыкания счётных подмножеств \mathpdf{N}{N}]{Замыкания счётных подмножеств~\(N\) и~кардинальные инварианты пространства Белла}
\label{sec:Bell_space:general}

Напомним, что в~пространстве~\(\beta\omega\) замыкание любого бесконечного подмножества~\(\omega\) гомеоморфно~\(\beta\omega\). Мы~покажем, что существуют бесконечные подмножества~\(N\), замыкания которых в~\(BN\) гомеоморфны~\(\beta\omega\), и~бесконечные подмножества~\(N\), которые являются сходящимися последовательностями в~\(BN\).
\begin{theorem}
    \label{the:pi(M)=betaomega}
    %
    \textls[10]{Пусть~\(\pi(M)\) "--- строгая бесконечная антицепь,} и~множество~\(X = \{x_i \colon i \in M\}\) такое, что~\(x_i \in [C_{\pi(i)}]\). Тогда~\([X]\) гомеоморфно~\(\beta\omega\).
\end{theorem}
\begin{proof}
    Рассмотрим семейство~\(\lambda = \{C_{\pi(i)} \colon i \in M\}\).

    Для~ультрафильтра~\(\xi \in \beta M \setminus M\) и~\(F \in \xi\) определим:
    \begin{align*}
        W_F &= \l[\cup \l\{C_{\pi(i)} \colon i \in F\r\}\r],\\
        \lambda_\xi &= \l\{W_F \colon F \in \xi\r\},\\
        L_\xi &= \cap \l\{[W_F] \colon W_F \in \lambda_\xi\r\}.
    \end{align*}

    Легко видеть, что
    \begin{enumerate}
        \item \(L_\xi\cap L_\eta = \varnothing\) для~\(\xi \neq \eta\);
        \item \(\{X \cap W_F \colon F \in \xi\r\}\) "--- ультрафильтр на~множестве~\(X\);
        \item \(|L_\xi \cap [X]| = 1\).
    \end{enumerate}

    Пусть~\(x_\xi = \cap \{[X \cap W_F] \colon F \in \xi\}\). Из~конструкции следует, что~\(X \cup \{x_\xi \colon \xi \in \beta M \setminus M\} = [X]\) гомеоморфно~\(\beta\omega\).
\end{proof}

В~случае нароста эту теорему можно обобщить.
\begin{theorem}
    \label{the:betaomega_in_BN-N}
    %
    Пусть~\(\{s_i \colon i \in \omega\}\) "--- антицепь, и~\(X = \{x_i \colon i \in \omega\}\) такое множество, что~\(x_i \in (C_{s_i})^*\). Тогда~\([X]\) гомеоморфно~\(\beta\omega\).
\end{theorem}
Доказательство этой теоремы вытекает из~того, что
\[
    C_s^* = \cup \l\{C_t^* \colon \dom t = \dom s + 1,\ s \< t\r\}
    \quad\text{для любого}\ s \in N.
\]
\textls[-10]{Таким образом, мы~можем построить строгую антицепь~\(\{s_i' \colon i \in \omega\}\) такую, что~\(x_i \in C_{s_i'}^* \subseteq C_{s_i}^*\).}

\goodbreak

Из~этой теоремы и~свойств пространства~\(\beta\omega\) мы~получаем
\begin{corollary}
    \label{cor:BN_cardinals}
    ~
    \begin{enumerate}[\upshape 1.]
        \item \(\weight(BN) = 2^\omega\);
        \item \(\spread(BN) = 2^\omega\);
        \item \(\tightness(BN) = 2^\omega\);
        \item \(|BN| = 2^{2^\omega}\).
    \end{enumerate}
\end{corollary}

\textls[-7]{Естественно возникает вопрос о~существовании подмножеств~\(N\), замыкания которых не~гомеоморфны~\(\beta\omega\).} Рассмотрим два примера антицепей из~\(N\), замыкания которых не~гомеоморфны~\(\beta\omega\). Для~доказательства того, что замыкание счётного дискретного множества не~гомеоморфно~\(\beta\omega\), воспользуемся тем фактом, что для любых~\(A, B \subseteq \omega\), если~\(A \cap B = \varnothing\), то~\([A]_{\beta\omega} \cap [B]_{\beta\omega} = \varnothing\).

Для~\(s \in N\) обозначим:
\[
    C_s' = \{
        t \in N \colon \dom t = \dom s + 1, t|_{\dom s} = s
    \}.
\]

\begin{example}
    \label{ex:01}
    %
    Пусть~\(f \in P\) и~\(\{s_n = f|_{n + 1} \colon n \in \omega\}\) и~множества~\(A_n, B_n \subseteq C_{s_n}' \setminus \{s_{n+1}\}\) такие, что~\(A_n \cap B_n = \varnothing\) и~\(|A_n| = |B_n| =\break= \lfloor n / 2 \rfloor\) (где~\(\lfloor n / 2 \rfloor\) "--- целая часть числа~\(n / 2\)) для всех~\(n \in \omega\). Обозначим~\(A = \cup \{A_n \colon n \in \omega\}\) и~\(B = \cup \{B_n \colon n \in \omega\}\).

    Множество~\(A \cup B\) является антицепью. Но~при этом замыкание этого множества не~гомеоморфно~\(\beta\omega\).

    Пусть~\(x = \lim\limits_{n \to \infty} s_n\) и~\(Ox = [C_{\pi|M} \setminus \cupl{i \< k} C_{\pi_i}]\) "--- некоторая окрестность точки~\(x\). Так~как~\(x\) "--- предел~\(\{s_n \colon n \in \omega\}\), то~существует~\(n_0 \in \omega\) такое, что для любого~\(n > n_0\) точка~\(s_n \in O_x\) и~\(|A_n| > k\), \(|B_n| > k\). Множество~\(\cupl{i \< k} C_{\pi_i}\) содержит не~более чем~\(k\) точек из~\(C_{s_n}'\), следовательно,~\(Ox \cap A_n \neq \varnothing\) и~\(Ox \cap B_n \neq \varnothing\). Таким образом,~\(x \in [A] \cap [B] \neq \varnothing\), а~следовательно,~\([A \cup B]\) не~гомеоморфно~\(\beta\omega\).
\end{example}
\begin{example}
    \label{ex:02}
    %
    Пусть~\(\{\pi(n) \colon n \in \omega\}\) "--- строгая антицепь и~множества~\(A_n, B_n \subseteq C_{\pi(n)}'\) такие, что~\(A_n \cap B_n = \varnothing\) и~\(|A_n| = |B_n| = \lfloor n / 2 \rfloor\). Обозначим~\(A = \cup \{A_n \colon n \in \omega\}\) и~\(B = \cup \{B_n \colon n \in \omega\}\).

    Как~и~в~предыдущем примере, множество~\(A \cup B\) является антицепью и~его замыкание не~гомеоморфно~\(\beta\omega\).

    Рассмотрим точку~\(x \in [\{\pi(n) \colon n \in \omega\}]\). По~теореме~\ref{the:pi(M)=betaomega} замыкание антицепи гомеоморфно~\(\beta\omega\). Таким образом можно рассматривать~\(x\), как свободный ультрафильтр на~нашей антицепи. Рассмотрим произвольную окрестность~\(Ox = [C_{\pi'|M'} \setminus \cupl{i \< k} C_{\pi_i}]\) точки~\(x\). Так~как~\(x\) "--- свободный ультрафильтр на~нашей антицепи, то
    \[
        \l|Ox \cap \l\{\pi(n) \colon n \in \omega\r\}\r| = \omega.
    \]
    Тогда найдётся бесконечно много~\(n \in \omega\) таких, что~\(\pi(n) \in Ox\) и~\(|A_n| > k\), \(|B_n| > k\). Множество~\(\cupl{i \< k} C_{\pi_i}\) содержит не~более чем~\(k\) точек из~\(C_{s_n}'\), следовательно~\(Ox \cap A_n \neq \varnothing\) и~\(Ox \cap B_n \neq \varnothing\). Таким образом,~\(x \in [A] \cap [B] \neq \varnothing\), а~следовательно,~\([A \cup B]\) не~гомеоморфно~\(\beta N\).
\end{example}

С~другой стороны, как уже было сказано выше, в~\(N\) существуют бесконечные множества, являющиеся сходящимися последовательностями в~\(BN\).
\begin{theorem}
    \label{the:lim}
    %
    Пусть~\(A = \{s_i \colon i \in \omega\}\) "--- бесконечная цепь из~\(N\). Тогда~\(A\) является сходящейся последовательностью в~\(BN\).
\end{theorem}
\begin{proof}
    Пусть~\(A = \{s_i \colon i \in \omega\}\) "--- бесконечная цепь в~\(N\), то~есть~\(s_i < s_{i + 1}\) для всех~\(i \in \omega\). Пусть~\(x \in [A] \setminus A\) и
    \[
        O_x = \L[C_{\pi|M} \setminus \cupl{i \< n} C_{\pi_i}\R]
    \]
    базисная окрестность точки~\(x\).

    Найдётся точка~\(s_{i_0} \in A\) такая, что~\(s_{i_0} \in C_{\pi|M}\) и, следовательно, \(s_i \in C_{\pi|M}\) для всех~\(i \> i_0\). С~другой стороны, мы~имеем:
    \[
        \cupl{i \< n} C_{\pi_i} \cap A = \varnothing,
    \]
    так как в~противном случае~\(\cupl{i \< n} C_{\pi_i}\) содержало~бы всё множество~\(A\), за~исключением конечного числа точек.

    \textls[-7]{Итак, \(x\) предел сходящейся последовательности~\(A = \{s_i \colon i \in \omega\}\).}
\end{proof}

Следующая теорема показывает насколько много в~\(BN\) пределов цепей и~копий~\(\beta\omega\).
\begin{theorem}
    \label{the:pi(M)&lim-many}
    %
    Пусть
    \begin{gather*}
        Q = \l\{
            x \colon x\ \text{предел сходящейся последовательности точек~\(N\)}
        \r\},\\
        \mu = \l\{
            A^* \colon A \subseteq N, [A]
            \ \text{гомеоморфно~\(\beta\omega\)}
        \r\}.
    \end{gather*}
    Тогда~\(Q\) всюду плотно в~\(BN \setminus N\) и~\(\mu\) образует~\(\pi\);-сеть в~\(BN \setminus N\).
\end{theorem}
\begin{proof}
    \textls[-10]{Пусть~\(V = C_{\pi|M} \setminus \cupl{i \< m} C_{\pi_i}\) "--- бесконечный элемент~\(\Gamma\). По~индукции построим две последовательности~\(\{s_k \colon k \in \omega\}\) и~\(\{t_k \colon k \in \omega\}\) в~\(V\) такие, что}
    \begin{enumerate}
        \item \(\{s_k \colon k \in \omega\}\) "--- цепь в~\(N\);
        \item \(\{t_k \colon k \in \omega\}\) "--- строгая антицепь в~\(N\);
        \item \(s_k < t_{k + 1}\) для всех~\(k \in \omega\).
    \end{enumerate}

    Пусть~\(n_0\) и~\(s \in N\) такие, что~\(n_0 \> m + 1\) и~\(\dom s = n_0 + 1\), \(s \in C_{\pi|M} \setminus \cupl{i \< m} C_{\pi_i}\). Определим~\(s_0 = t_0 = s\).

    Пусть мы~определили~\(\{s_k \colon k \< \ell\}\) и~\(\{t_k \colon k \< \ell\}\), удовлетворяющие условиям~1--3. Поскольку~\(\ell \> n_0 \> m + 1\), существует не~менее двух продолжений~\(s_\ell\), лежащих в~\(C_{\pi|M} \setminus \cupl{i \< m} C_{\pi_i}\). мы~определим одно из~них как~\(s_{\ell + 1}\), а~другое, как~\(t_{\ell + 1}\).

    Итак, мы~имеем~\(\{s_k \colon k \in \omega\}\) и~\(\{t_k \colon k \in \omega\}\). По~конструкции, \(\{s_k \colon k \in \omega\} \subseteq V\), \(\{s_k \colon k \in \omega\}\) "--- сходящаяся последовательность и~\(\lim\limits_{k \to \infty} s_k \in V^*\), \(\{t_k \colon k \in \omega\} \subseteq V\), \([\{t_k \colon k \in \omega\}]\) гомеоморфно~\(\beta\omega\) и~\((\{t_k \colon k \in \omega\})^* \subseteq V^*\).
\end{proof}

В~пространстве~\(\beta\omega\) наросты любых двух почти не~пересекающихся множеств из~\(\omega\) не~пересекаются. В~случае пространства Белла ситуация другая.
\begin{theorem}
    \label{the:almost_disjoint_families}
    Существуют семейства почти не~пересекающихся подмножеств~\(N\):
    \begin{enumerate}[\normalfont 1.]
        \item
            \label{the:almost_disjoint_families:1}
            \(\lambda_1 = \{A_\alpha \colon \alpha \in 2^\omega\}\) такое, что~\([A_\alpha]\) гомеоморфно~\(\beta\omega\) для всех~\(\alpha \in 2^\omega\), \(A_\alpha^* \cap A_\beta^* = \varnothing\) для~\(\alpha \neq \beta\);
        \item
            \label{the:almost_disjoint_families:2}
            \(\lambda_2 = \{A_\alpha \colon \alpha \in 2^\omega\}\) такое, что~\(|A_\alpha^*| = 1\) и~\(A_\alpha^* = A_\beta^*\) для всех~\(\alpha, \beta \in 2^\omega\);
        \item[\upshape 2\(\lefteqn{\mathstrut'}.\)]
            \label{the:almost_disjoint_families:2'}
            \(\lambda_2' = \{A_\alpha \colon \alpha \in 2^\omega\}\) такое, что~\(|A_\alpha^*| = 1\) для всех~\(\alpha \in 2^\omega\), \(A_\alpha^* \cap A_\beta^* = \varnothing\) для~\(\alpha \neq \beta\);
        \item
            \label{the:almost_disjoint_families:3}
            \(\lambda_3 = \{A_\alpha \colon \alpha \in 2^\omega\}\) такое, что~\(A_\alpha^* = BN \setminus N\) для всех~\(\alpha \in 2^\omega\).
    \end{enumerate}
\end{theorem}
\begin{proof}
    Пусть~\(\theta = \{F_\alpha \colon \alpha \in 2^\omega\}\) "--- семейство почти не~пересекающихся бесконечных подмножеств~\(\omega\).

    \textbf{\ref{the:almost_disjoint_families:1}}\quad Пусть~\(A_1 = \{s_i \colon i \in \omega\}\) такое, что~\([A_1]\) гомеоморфно~\(\beta\omega\). Определим~\(A_1(F_\alpha) = \{s_i \colon i \in F_\alpha\}\).

    Семейство~\(\lambda_1 = \{A_1(F_\alpha) \colon \alpha \in 2^\omega\}\) удовлетворяет условию~\ref{the:almost_disjoint_families:1}.

    \textbf{\ref{the:almost_disjoint_families:2}}\quad Аналогичным образом из~сходящейся последовательности~\(A_2=\l\{s_i:i\in\omega\r\}\) мы~можем получить семейство
    \[
        \lambda_2=\l\{A_2(F_\alpha):\alpha\in2^\omega\r\},
    \]
    удовлетворяющее условию~\ref{the:almost_disjoint_families:2}.

    \textbf{\ref{the:almost_disjoint_families:2'}\(\mathstrut'\)}\quad Для~доказательства этого мы~можем рассмотреть почти не~пересекающееся семейство сходящихся последовательностей в~\(N\). Его~мощность совпадает с~мощностью множества
    \[
        P = \{f \in \omega^\omega \colon f(n) \< n + 1\}
    \]
    и~равна~\(2^\omega\).

    \textbf{\ref{the:almost_disjoint_families:3}}\quad Пусть~\(A_\alpha = \{s \in N \colon \dom s = n\ \text{для всех~\(n \in F_\alpha\)}\}\). Очевидно, что~\(\lambda_3 = \{A_\alpha \colon \alpha \in 2^\omega\}\) удовлетворяет условию~\ref{the:almost_disjoint_families:3}.
\end{proof}

А.\,Грызловым в~\cite{praga:2012} были доказаны следующие теоремы.
\begin{theorem}
    \label{the:con=>chain}
    %
    \textls[-15]{Если множество~\(A \subseteq N\) такое, что~\(|[A] \setminus A| = 1\), тогда существует конечное множество~\(K \subseteq A\) такое, что~\(A \setminus K\) "--- цепь.}
\end{theorem}
\begin{theorem}
    \label{the:betaomega=>antichain}
    %
    Если замыкание~\([A]\) множества~\(A \subseteq N\) "--- копия~\(\beta\omega\), тогда~\(A\) "--- объединение конечного числа антицепей.
\end{theorem}
\begin{corollary}
    \label{cor:2.1.2}
    %
    Если замыкание~\([A]\) множества~\(A \subseteq N\) "--- копия~\(\beta\omega\), тогда длины всех цепей, входящих в~\(A\) ограничены одной константой.
\end{corollary}

Исходя из~этого, для изучения свойств сходящихся последовательностей, лежащих в~\(N\), достаточно рассматривать только цепи. Поэтому в~дальнейшей работе мы~будем рассматривать цепи и~антицепи.
