#!/usr/bin/env
# -*- coding: utf-8 -*-

import os

from fabric.api import task, local

try:
    from gntp.notifier import mini as note

except ImportError:
    def note(*args):
        pass

BUILD_ARTEFACTS = (
    '*.aux',
    '*.bbl',
    '*.blg',
    '*.dvi',
    '*.fls',
    '*.idx',
    '*.ilg',
    '*.ind',
    '*.log',
    '*.out',
    '*.pdf',
    '*.ps',
    '*.toc',
    '*.upa',
    '*.upb',
)

RECEPIENTS = (
    'gryzlov@udsu.ru',
    'rpa4@bk.ru'
)


@task
def clean():
    local('rm -rf {0}'.format(' '.join(BUILD_ARTEFACTS)))


@task
def build():
    local('latexmk -pdf main')


@task
def email():
    from ConfigParser import ConfigParser

    from gmail import GMail, Message

    config = ConfigParser()
    config.read(os.path.join(os.environ['HOME'], '.about.me'))

    gmail = GMail(config.get('logins', 'google').strip('"\''),
                  config.get('passwords', 'google').strip('"\''))

    msg = Message('Учебное пособие 2014/11', ','.join(RECEPIENTS),
                  attachments=['main.pdf'])
    gmail.send(msg)
    gmail.close()
